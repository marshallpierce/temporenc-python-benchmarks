import benchmark

import temporenc


class Benchmark_Serialize(benchmark.Benchmark):
    each = 10000

    def setUp(self):
        self.loop_size = 100

    def test_serialize_date(self):
        for x in xrange(0, self.loop_size):
            temporenc.packb(year=2014, month=10, day=23)

    def test_serialize_time(self):
        for x in xrange(0, self.loop_size):
            temporenc.packb(hour=12, minute=30, second=30)

    def test_serialize_date_time(self):
        for x in xrange(0, self.loop_size):
            temporenc.packb(year=2014, month=10, day=23, hour=12, minute=30, second=30)

    def test_serialize_date_time_offset(self):
        for x in xrange(0, self.loop_size):
            temporenc.packb(year=2014, month=10, day=23, hour=12, minute=30, second=30, tz_offset=120)

    def test_serialize_date_time_subsecond(self):
        for x in xrange(0, self.loop_size):
            temporenc.packb(year=2014, month=10, day=23, hour=12, minute=30, second=30, nanosecond=123456789)

    def test_serialize_date_time_subsecond_offset(self):
        for x in xrange(0, self.loop_size):
            temporenc.packb(year=2014, month=10, day=23, hour=12, minute=30, second=30, nanosecond=123456789,
                            tz_offset=120)


class Benchmark_Deserialize(benchmark.Benchmark):
    each = 10000

    def setUp(self):
        self.loop_size = 100
        self.serialized_date = temporenc.packb(year=2014, month=10, day=23)
        self.serialized_time = temporenc.packb(hour=12, minute=30, second=30)
        self.serialized_date_time = temporenc.packb(year=2014, month=10, day=23, hour=12, minute=30, second=30)
        self.serialized_date_time_offset = temporenc.packb(year=2014, month=10, day=23, hour=12, minute=30, second=30,
                                                           tz_offset=120)
        self.serialized_date_time_subsecond = temporenc.packb(year=2014, month=10, day=23, hour=12, minute=30,
                                                              second=30, nanosecond=123456789)
        self.serialized_date_time_subsecond_offset = temporenc.packb(year=2014, month=10, day=23, hour=12, minute=30,
                                                                     second=30, nanosecond=123456789, tz_offset=120)

    def test_deserialize_date(self):
        for x in xrange(0, self.loop_size):
            temporenc.unpackb(self.serialized_date)

    def test_deserialize_time(self):
        for x in xrange(0, self.loop_size):
            temporenc.unpackb(self.serialized_time)

    def test_deserialize_date_time(self):
        for x in xrange(0, self.loop_size):
            temporenc.unpackb(self.serialized_date_time)

    def test_deserialize_date_time_offset(self):
        for x in xrange(0, self.loop_size):
            temporenc.unpackb(self.serialized_date_time_offset)

    def test_deserialize_date_time_subsecond(self):
        for x in xrange(0, self.loop_size):
            temporenc.unpackb(self.serialized_date_time_subsecond)

    def test_deserialize_date_time_subsecond_offset(self):
        for x in xrange(0, self.loop_size):
            temporenc.unpackb(self.serialized_date_time_subsecond_offset)


if __name__ == '__main__':
    benchmark.main(format="markdown", numberFormat="%.4g")
