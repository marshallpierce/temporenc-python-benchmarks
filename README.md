Benchmark for the [Python implementation of Temporenc]().

Assuming you have virtualenvwrapper:

```
mkvirtualenv temporenc-python-benchmarks
workon temporenc-python-benchmarks
pip install -r requirements.txt
python temporenc_benchmark.py
```
